
# Structure
  - The project will consist of a daemon monitoring a region in memory
    - The deamon will block all tries to alter the region
  - A client communicating with the daemon, using ftp maybe?


# Possible Ideas
  - maybe mount the partition as read-only?
    - in this case the block of memory will have to be a seperate partition, *CON*
    - easy block all writes, *PRO*
    - how would the program write to it then?
    - it probably **has** to be a partition due to how filesystems work, 
      not doing that will be probably too much of a pain

# TODO as of 12/1
  - [X] decide on whether to follow a partition or a directory. **DONE: the program will follow a partition**
  - [ ] set up a vm for testing
  - [ ] make a simple daemon
  - [ ] figure out how to edit disk partitions in c
  - [ ] locate a region in the disk
  - [ ] block all attempts to alter that region
  - [ ] document

# Sources
  - [Getting Started With Qemu](https://drewdevault.com/2018/09/10/Getting-started-with-qemu.html)
  - [Creating A Linux Service](https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6)
  - [Trapping memory writes on linux](https://stackoverflow.com/questions/19457342/trying-to-trap-all-memory-reads-writes-on-a-linux-machine)

